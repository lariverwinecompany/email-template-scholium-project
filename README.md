
# Responsive Email Template for Scholium Project

## Notes 

Open up a HTML editor and change the template on your computer. I test it by opening it in a browser. Once I’m satisfied I then copy and paste in the HTML into VR to start a new image. Only thing that needs to be changed are the image urls. Upload images through the VR menu links for Tools, Library. Put the images in the folder named 2020. After you upload images into the library you can click on a photo to get the URL and use that to replace the path in the src tag in the email template. 

Be careful using Vertical Response's browser-based web editor for changing text or uploading images. 

## HTML Editors

Here are several HTML editors are available to download for free. 

- [atom.io](https://atom.io/)
- [vs code](https://code.visualstudio.com/)
- [bbedit](https://www.barebones.com/products/bbedit/)

## Text Editors

Use a plain text editor to compose letters and texts. Using one helps avoid formatting issues that are introduced when using Microsoft Word or Pages.  

Both of these are worth the money and work on iOS and MacOS. 

- [iA Writer](https://ia.net/writer)
- [Bear](https://bear.app/)

## Typography Cheatsheet

Jeremiah Shoaf at typewolf.com created a [typography cheatsheet](https://www.typewolf.com/cheatsheet) is a comprehensive guide to using proper typographic characters, including correct grammatical usage.

- [typography cheatsheet](https://www.typewolf.com/cheatsheet)

## GUI apps for Git

- [Git Tower](https://www.git-tower.com/mac)
- [GitHub Desktop](https://desktop.github.com/)


